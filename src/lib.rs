struct Node<T>
where
    T: Clone,
{
    value: T,
    next: Option<Box<Node<T>>>,
}

impl<T> Node<T>
where
    T: Clone,
{
    fn new(value: T) -> Self {
        Self {
            value,
            next: Option::None,
        }
    }
}

struct LinkedList<T>
where
    T: Clone,
{
    init: Option<Box<Node<T>>>,
}

impl<T> LinkedList<T>
where
    T: Clone,
{
    fn new() -> Self {
        Self { init: Option::None }
    }

    fn push(&mut self, value: T) {
        // create node
        let new_node = Box::new(Node::new(value));
        let mut actual_node: &mut Option<Box<Node<T>>> = &mut self.init;
        while actual_node.is_some() {
            actual_node = &mut actual_node.as_mut().unwrap().next;
        }
        *actual_node = Option::Some(new_node);
    }

    fn pop(&mut self) -> Option<T> {
        match &mut self.init {
            Some(node) => {
                let actual_value = Some(node.value.clone());
                self.init = node.next.take();
                actual_value
            }
            None => Option::None,
        }
    }

    fn peek(&self) -> Option<T> {
        match &self.init {
            Some(node) => Some(node.value.clone()),
            None => Option::None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_save_int() {
        let node = Node {
            value: 32,
            next: Option::None,
        };
        assert_eq!(node.value, 32i32)
    }

    #[test]
    fn it_creates_empty_list_and_push_values() {
        let mut linked_list: LinkedList<u128> = LinkedList::new();
        assert!(linked_list.init.is_none());
        linked_list.push(1);
        assert!(linked_list.init.is_some());
        assert_eq!(linked_list.peek(), Some(1));
        linked_list.push(2);
        assert_eq!(linked_list.peek(), Some(1));
        let value = linked_list.pop();
        assert_eq!(value, Some(1));
        assert_eq!(linked_list.peek(), Some(2));
        let value = linked_list.pop();
        assert_eq!(value, Some(2));
        assert_eq!(linked_list.peek(), None);
    }
}
